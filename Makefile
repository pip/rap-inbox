# Opciones
PORT ?= 3001
BIND ?= 127.0.0.1
RUN  ?= nobody

# Hacer que las opciones estén disponibles
export

rap-inbox: src/rap-inbox.cr shard.lock
	crystal build --release $<
	strip --strip-all $@

service := /etc/systemd/system/rap-inbox.service
$(service): contrib/rap-inbox.service.in
	test -w $(dir $@)
	envsubst < $< > $@
	systemctl daemon-reload

service: $(service)
