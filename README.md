# rap-inbox

Recibe archivos de hosts de tinc para la red autónoma pirata.

## Compilación

Instalar Crystal 0.35 o posterior.

```bash
make rap-inbox
```

## Instalación

Clonar este repositorio en un directorio que pueda ser escrito por
`nobody` o algún usuario del sistema sin otros privilegios.

```bash
git clone https://0xacab.org/pip/rap-inbox /srv/http/rap.partidopirata.com.ar
chown -R nobody:nobody /srv/http/rap.partidopirata.com.ar
```

Luego instalar y habilitar el servicio:

```
cd /srv/http/rap.partidopirata.com.ar
make service
systemctl enable --now rap-inbox.service
```

## Uso

RAP Inbox está pensadx para alojarse en un nodo público de la RAP, de
forma que los nodos privados le puedan enviar sus llaves públicas.

Recomendamos alojar detrás de un proxy reverso como Nginx con un límite
de 1KB para archivos subidos.  Hay una configuración de muestra en
`contrib/nginx.conf`.

```
./rap-inbox &
curl -F host=@hosts/nufla http://localhost:3000/nufla
```
