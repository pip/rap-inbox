require "kemal"
require "file_utils"

# No queremos filtrar las llaves!
serve_static false

# Acepta llaves de tinc por post a menos que ya existan
post "/:network/:host" do |env|
  HTTP::FormData.parse(env.request) do |u|
    network = env.params.url["network"]
    host = env.params.url["host"]

    # Sólo letras, números y guiones bajos
    unless host =~ /\A[a-z0-9_]+\z/i
      Log.info { "#{host} no sigue el formato de nombre de nodo, ignorando" }

      halt env, status_code: 403, response: "Forbidden"
    end

    unless network =~ /\A[a-z0-9]+\z/i
      Log.info { "#{network} no sigue el formato de nombre de red, ignorando" }

      halt env, status_code: 403, response: "Forbidden"
    end

    path = File.join [Kemal.config.public_folder, "networks", network, "hosts", host]

    # No aceptar llaves que ya existan
    if File.exists? path
      Log.info { "#{path} ya existe, ignorando" }

      halt env, status_code: 403, response: "Forbidden"
    end

    # Crea el directorio si no existe
    FileUtils.mkdir_p File.dirname(path)

    # Copiar el archivo al destino
    File.open path, "w" do |f|
      IO.copy u.body, f
    end

    Log.info { "#{host} creado en #{network}" }

    # Responder con 201
    halt env, status_code: 201, response: "Created"
  end
end

Kemal.run
